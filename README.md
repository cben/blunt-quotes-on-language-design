# Blunt quotes on programming language design

I enjoy blunt statements about programming languages, so decided to start collecting them.  Contributions welcome.

*All emphasis mine* unless said otherwise.

> ###### Numbers
> Pyret *has numbers*, because we believe an 8GB machine should not limit students to using just 32 bits.  
>
> — https://www.pyret.org/
>
> Not true in Java, true in Pyret: `((1 / 3) * 3) == 1`

> Solving a programming problem requires choosing the right concepts.  
> *All but the smallest toy problems require different sets of concepts for different parts.*  
> This is why programming languages should support many paradigms.
>
> — [Programming paradigms for dummies: what every programmer should know](https://www.info.ucl.ac.be/~pvr/VanRoyChapter.pdf), Peter Van Roy, 2009

> - functions are closures;
> - procedures are closures;
> - objects are closures;
> - classes are closures;
> - software components are closures.
>
> Many abilities normally associated with specific paradigms are based on closures:
> - Instantiation and genericity, normally associated with object-oriented programming, can be done easily by writing functions that return other functions. *In
object-oriented programming the first function is called a “class” and the second is
called an “object”.*
>
> — [Programming paradigms for dummies: what every programmer should know](https://www.info.ucl.ac.be/~pvr/VanRoyChapter.pdf), Peter Van Roy, 2009
